package com.mchs.gamescripting;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GamescriptingApplication {

	public static void main(String[] args) {
		SpringApplication.run(GamescriptingApplication.class, args);
	}

}
